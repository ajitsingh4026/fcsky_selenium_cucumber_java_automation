package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DivisionalUserUI extends UserUI {

	@FindBy(id = "ms-parentdivision")
	public WebElement division_MultiSelect;

	public DivisionalUserUI(WebDriver driver) {
		super(driver);
	}
}
