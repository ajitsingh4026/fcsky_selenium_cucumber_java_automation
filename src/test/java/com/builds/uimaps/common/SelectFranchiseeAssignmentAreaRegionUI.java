package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SelectFranchiseeAssignmentAreaRegionUI {
	
	@FindBy(xpath=".//input[@type='radio' and @value='1']")
	public WebElement assignFranchiseeslater_radioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='2']")
	public WebElement AutomaticallyAssignFranchiseesFallingInThisAreaRegion_radioBtn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='3']")
	public WebElement ManuallyAssignEachFranchiseeToThisAreaRegion_radioBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Next']")
	public WebElement NextBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement CancelBtn;
	
	
	
	public SelectFranchiseeAssignmentAreaRegionUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
