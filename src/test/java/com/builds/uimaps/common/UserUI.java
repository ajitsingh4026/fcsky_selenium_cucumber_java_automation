package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserUI {

	@FindBy(id = "userName")
	public WebElement userName;

	@FindBy(xpath = ".//input[@id='password' and @type='password']")
	public WebElement password;

	@FindBy(xpath = ".//input[@id='confirmPassword' and @type='password']")
	public WebElement confirmPassword;

	@FindBy(id = "ms-parentroleID")
	public WebElement role_MultiSelect;

	@FindBy(id = "userType")
	public WebElement userType;

	@FindBy(id = "timezone")
	public WebElement timezone_Select;

	@FindBy(id = "isDaylight")
	public WebElement isDaylight_Check;

	@FindBy(id = "jobTitle")
	public WebElement jobTitle;

	@FindBy(id = "userLanguage")
	public WebElement userLanguage;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "zipcode")
	public WebElement zipcode;

	@FindBy(id = "state")
	public WebElement state;

	@FindBy(id = "phone1")
	public WebElement phone1;

	@FindBy(id = "phoneExt1")
	public WebElement phoneExt1;

	@FindBy(id = "phone2")
	public WebElement phone2;

	@FindBy(id = "phoneExt2")
	public WebElement phoneExt2;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "email")
	public WebElement email;

	@FindBy(id = "loginUserIp")
	public WebElement loginUserIp;

	@FindBy(id = "isBillable")
	public WebElement isBillable_Check;

	@FindBy(id = "auditor")
	public WebElement consultant_Check;

	@FindBy(id = "userPictureName")
	public WebElement userPictureName;

	@FindBy(id = "sendNotification")
	public WebElement sendNotification;

	@FindBy(id = "Submit")
	public WebElement submit;

	public UserUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
