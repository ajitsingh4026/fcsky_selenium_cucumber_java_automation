package fcsky.sales.stepdefinitions;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.builds.uimaps.common.LoginPage;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.runtime.ScenarioImpl;
import fcsky.automation.TestInitiator;
import fcsky.utils.methods.BaseTest;
import fcsky.utils.methods.TestCaseFailed;

public class SalesStepDefinitions implements BaseTest {
	protected WebDriver driver = TestInitiator.getDefaultDriver("chrome");
	//Navigation Steps
	LoginPage lp=new LoginPage();
	//Step to navigate to specified URL
	@Then("^I navigate to \"([^\"]*)\"$")
	public void navigate_to(String link)
	{
		navigationObj.navigateTo(link);
	}
	
	//Step to navigate forward
	@Then("^I navigate forward")
	public void navigate_forward()
	{
		navigationObj.navigate("forward");
	}
	
	//Step to navigate backward
	@Then("^I navigate back")
	public void navigate_back()
	{
		navigationObj.navigate("back");
	}
	
	// steps to refresh page
	@Then("^I refresh page$")
	public void refresh_page()
	{
		driver.navigate().refresh();
	}
	
	// Switch between windows
	
	//Switch to new window
	@Then("^I switch to new window$")
	public void switch_to_new_window()
	{
		navigationObj.switchToNewWindow();
	}
	
	//Switch to old window
	@Then("^I switch to previous window$")
	public void switch_to_old_window()
	{
		navigationObj.switchToOldWindow();
	}
	
	//Switch to new window by window title
	@Then("^I switch to window having title \"(.*?)\"$")
	public void switch_to_window_by_title(String windowTitle) throws Exception
	{
		navigationObj.switchToWindowByTitle(windowTitle);
	}
	
	//Close new window
	@Then("^I close new window$")
	public void close_new_window()
	{
		navigationObj.closeNewWindow();
	}
	
	// Switch between frame
	
	// Step to switch to frame by web element
	@Then("^I switch to frame having (.+) \"(.*?)\"$")
	public void switch_frame_by_element(String method, String value)
	{
		navigationObj.switchFrame(method, value);
	}
	
	// step to switch to main content
	@Then("^I switch to main content$")
	public void switch_to_default_content()
	{
		navigationObj.switchToDefaultContent();
	}
	
	// To interact with browser
	
	// step to resize browser
	@Then("^I resize browser window size to width (\\d+) and height (\\d+)$")
	public void resize_browser(int width, int heigth)
	{
		navigationObj.resizeBrowser(width, heigth);
	}
	
	// step to maximize browser
	@Then("^I maximize browser window$")
	public void maximize_browser()
	{
		navigationObj.maximizeBrowser();
	}
	
	//Step to close the browser
	@Then("^I close browser$")
	public void close_browser()
	{
		navigationObj.closeDriver();
	}
	
	// zoom in/out page
	
	// steps to zoom in page
	@Then("^I zoom in page$")
	public void zoom_in()
	{
		navigationObj.zoomInOut("ADD");
	}
	
	// steps to zoom out page
	@Then("^I zoom out page$")
	public void zoom_out()
	{
		navigationObj.zoomInOut("SUBTRACT");
	}
	
	// zoom out webpage till necessary element displays
	
	// steps to zoom out till element displays
	@Then("^I zoom out page till I see element having (.+) \"(.*?)\"$")
	public void zoom_till_element_display(String type, String accessName) throws Exception
	{
		miscmethodObj.validateLocator(type);
		navigationObj.zoomInOutTillElementDisplay(type,"substract", accessName);
	}
	
	// reset webpage view use
	
	@Then("^I reset page view$")
	public void reset_page_zoom()
	{
		navigationObj.zoomInOut("reset");
	}
	
	// scroll webpage
	
	@Then("^I scroll to (top|end) of page$")
	public void scroll_page(String to) throws Exception
	{
		navigationObj.scrollPage(to);
	}
	
	
	// scroll webpage to specific element
	
	@Then("^I scroll to element having (.+) \"(.*?)\"$")
	public void scroll_to_element(String type, String accessName) throws Exception
	{
		miscmethodObj.validateLocator(type);
		navigationObj.scrollToElement(type, accessName);
	}
	
	// hover over element
	
	// Note: Doesn't work on Windows firefox
	@Then("^I hover over element having (.+) \"(.*?)\"$")
	public void hover_over_element(String type, String accessName) throws Exception
	{
		miscmethodObj.validateLocator(type);
		navigationObj.hoverOverElement(type, accessName);
	}
	
	//Assertion steps
	
	/** page title checking
	 * @param present :
	 * @param title :
	 */
	@Then("^I should\\s*((?:not)?)\\s+see page title as \"(.+)\"$")
	public void check_title(String present,String title) throws TestCaseFailed
	{
		//System.out.println("Present :" + present.isEmpty());
		assertionObj.checkTitle(title,present.isEmpty());
	}
	
	// step to check element partial text
	@Then("^I should\\s*((?:not)?)\\s+see page title having partial text as \"(.*?)\"$")
	public void check_partial_text(String present, String partialTextTitle) throws TestCaseFailed
	{
		//System.out.println("Present :" + present.isEmpty());
		assertionObj.checkPartialTitle(partialTextTitle, present.isEmpty());
	}
	
	// step to check element text
	@Then("^element having (.+) \"([^\"]*)\" should\\s*((?:not)?)\\s+have text as \"(.*?)\"$")
	public void check_element_text(String type, String accessName,String present,String value) throws Exception
	{
		miscmethodObj.validateLocator(type);
		assertionObj.checkElementText(type, value, accessName,present.isEmpty());
	}
	
	//step to check element partial text
	@Then("^element having (.+) \"([^\"]*)\" should\\s*((?:not)?)\\s+have partial text as \"(.*?)\"$")
	public void check_element_partial_text(String type,String accessName,String present,String value) throws Exception
	{
		miscmethodObj.validateLocator(type);
		assertionObj.checkElementPartialText(type, value, accessName, present.isEmpty());
	}
	
	// step to check attribute value
	@Then("^element having (.+) \"([^\"]*)\" should\\s*((?:not)?)\\s+have attribute \"(.*?)\" with value \"(.*?)\"$")
	public void check_element_attribute(String type,String accessName,String present,String attrb,String value) throws Exception
	{
		miscmethodObj.validateLocator(type);
		assertionObj.checkElementAttribute(type, attrb, value, accessName, present.isEmpty());
	}
	
	// step to check element enabled or not
	@Then("^element having (.+) \"([^\"]*)\" should\\s*((?:not)?)\\s+be (enabled|disabled)$")
	public void check_element_enable(String type, String accessName,String present,String state) throws Exception
	{
		miscmethodObj.validateLocator(type);
		boolean flag = state.equals("enabled");
		if(!present.isEmpty())
		{
			flag = !flag;
		}
		assertionObj.checkElementEnable(type, accessName, flag);
	}
	
	//step to check element present or not
	@Then("^element having (.+) \"(.*?)\" should\\s*((?:not)?)\\s+be present$")
	public void check_element_presence(String type,String accessName,String present) throws Exception
	{
		miscmethodObj.validateLocator(type);
		assertionObj.checkElementPresence(type, accessName, present.isEmpty());
	}
	
	//step to assert checkbox is checked or unchecked
	@Then("^checkbox having (.+) \"(.*?)\" should be (checked|unchecked)$")
	public void is_checkbox_checked(String type, String accessName,String state) throws Exception
	{
		miscmethodObj.validateLocator(type);
		boolean flag = state.equals("checked");
		assertionObj.isCheckboxChecked(type, accessName, flag);
	}
	
	//steps to assert radio button checked or unchecked
	@Then("^radio button having (.+) \"(.*?)\" should be (selected|unselected)$")
	public void is_radio_button_selected(String type,String accessName,String state) throws Exception
	{
		miscmethodObj.validateLocator(type);
		boolean flag = state.equals("selected");
		assertionObj.isRadioButtonSelected(type, accessName, flag);
	}
	
	//steps to assert option by text from radio button group selected/unselected
	@Then("^option \"(.*?)\" by (.+) from radio button group having (.+) \"(.*?)\" should be (selected|unselected)$")
	public void is_option_from_radio_button_group_selected(String option,String attrb,String type,String accessName,String state) throws Exception
	{
		miscmethodObj.validateLocator(type);
		boolean flag = state.equals("selected");
		assertionObj.isOptionFromRadioButtonGroupSelected(type,attrb,option,accessName,flag);
	}
	
	//steps to check link presence
	@Then("^link having text \"(.*?)\" should\\s*((?:not)?)\\s+be present$")
	public void check_element_presence(String accessName,String present) throws TestCaseFailed, Exception
	{
		assertionObj.checkElementPresence("linkText",accessName,present.isEmpty());
	}
	
	//steps to check partail link presence
	@Then("^link having partial text \"(.*?)\" should\\s*((?:not)?)\\s+be present$")
	public void check_partial_element_presence(String accessName,String present) throws TestCaseFailed, Exception
	{
		assertionObj.checkElementPresence("partialLinkText", accessName, present.isEmpty());
	}
	
	//step to assert javascript pop-up alert text
	@Then("^I should see alert text as \"(.*?)\"$")
	public void check_alert_text(String actualValue) throws TestCaseFailed
	{
		assertionObj.checkAlertText(actualValue);
	}
	

	
	@Given("^I enter username \"([^\"]*)\" and password \"([^\"]*)\" into input field having$")
	public void i_enter_username_and_password_into_input_field_having(String username, String password) throws Throwable {
		LoginPage lp=new LoginPage();
		basePage.enterText(lp.userid,username);
		basePage.enterText(lp.password,password);
		basePage.clickOn(lp.loginBtBtn);
	}
	

	
	// @After
	// public final void tearDown() {
	// 	DriverUtil.closeDriver();
	// }
}