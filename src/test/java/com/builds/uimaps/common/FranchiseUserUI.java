package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FranchiseUserUI extends UserUI {

	@FindBy(id = "users")
	public WebElement user_Select;

	@FindBy(id = "salutation")
	public WebElement salutation_Select;

	@FindBy(name = "canAddEmployee")
	public WebElement manageOtherUsers_Check;

	public FranchiseUserUI(WebDriver driver) {
		super(driver);
	}
}
