package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Search {
	
	@FindBy(xpath=".//a[@qat_tabname='News']")
	public WebElement newsTab;
	
	@FindBy(xpath=".//a[@qat_tabname='Documents']")
	public WebElement documentTab;
	
	@FindBy(xpath=".//input[@name='search1' and @value='1' and @type='radio']")
	public WebElement documentRadioBtn;
	
	@FindBy(xpath=".//input[@value='Search']")
	public WebElement searchBtn;
	
	@FindBy(xpath=".//input[@value='Reset']")
	public WebElement resetBtn;
	
	@FindBy(xpath=".//input[@value='Cancel']")
	public WebElement cancelBtn;
	
	@FindBy(id="searchAllDocument")
	public WebElement searchAllDocument;
	
	@FindBy(xpath=".//img[@title='Search All Documents']")
	public WebElement searchAllDocumentIcon;
	
	@FindBy(xpath=".//a[@qat_tabname='Alerts']")
	public WebElement alertTab;
	
	@FindBy(xpath=".//a[@qat_tabname='Users']")
	public WebElement usersTab;
	
	@FindBy(xpath=".//a[@qat_tabname='FAQ']")
	public WebElement faqTab;
	
	@FindBy(id="fromDate")
	public WebElement fromDate;
	
	@FindBy(id="toDate")
	public WebElement toDate;
	
	@FindBy(name="searchRange")
	public WebElement searchRangeCombo;
	
	@FindBy(name="searchField")
	public WebElement searchIn;
	
	@FindBy(name="folder")
	public WebElement searchFolder;
	
	@FindBy(id="dateFrom")
	public WebElement dateFrom;
	
	@FindBy(id="dateTo")
	public WebElement dateTo;
	
	// Alerts Tab
	@FindBy(id="fromUser")
	public WebElement fromUser;
	
	@FindBy(name="searchAlertWith")
	public WebElement searchAlertWith;
	
	@FindBy(name="alertType")
	public WebElement alertType;
	
	@FindBy(name="alertCategory")
	public WebElement alertCategory;
	
	@FindBy(xpath=".//input[@name='subchk' and @value='y']")
	public WebElement subjectCheckbox;
	
	@FindBy(xpath=".//input[@name='detchk' and @value='y']")
	public WebElement textCheckbox;
	
	// Document Tab
	@FindBy(xpath=".//input[@name='search1' and @value='3']")
	public WebElement bothRadio;
	
	@FindBy(xpath=".//input[@name='search1' and @value='1']")
	public WebElement documentRadio;
	
	@FindBy(xpath=".//input[@name='search1' and @value='2']")
	public WebElement foldersRadio;
	
	@FindBy(name="searchWord")
	public WebElement documentSearchWord;
	
	@FindBy(name="exact")
	public WebElement exactCheckbox;
	
	// News Tab
	@FindBy(id="searchTitle")
	public WebElement searchWords;
	
	@FindBy(name="category")
	public WebElement newsCategory;
	
	// Users Tab
	@FindBy(name="ulevel")
	public WebElement userType;
	
	@FindBy(id="firstName")
	public WebElement firstName;
	
	@FindBy(id="lastName")
	public WebElement lastName;
	
	@FindBy(id="userID")
	public WebElement userID;
	
	@FindBy(id="city")
	public WebElement city;
	
	@FindBy(id="country")
	public WebElement country;
	
	@FindBy(id="zip")
	public WebElement zip;
	
	@FindBy(id="stateID")
	public WebElement state;
	
	@FindBy(id="phone")
	public WebElement phone;
	
	@FindBy(id="emailID")
	public WebElement emailID;
	
	// FAQ Tab
	@FindBy(id="searchFaq")
	public WebElement searchFaq;
	
	@FindBy(name="category")
	public WebElement faqCategory;
	
	public Search(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
}
