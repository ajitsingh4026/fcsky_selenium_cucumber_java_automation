package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FeedbackPage {

	public FeedbackPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "franchiseeNo")
	public WebElement drpFranchiseLocation;

	@FindBy(id = "fimComplaint_0fimTtComplaint")
	public WebElement txtComplaint;

	@FindBy(id = "fimComplaint_0fimCbComplaintType")
	public WebElement drpType;

	@FindBy(id = "fimComplaint_0fimDdIncidentDate")
	public WebElement drpComplaintDate;

	@FindBy(id = "fimComplaint_0fimTaSummary")
	public WebElement txtComplaintSummary;

	@FindBy(id = "fimDocuments_fimComplaint0fimDocumentTitle")
	public WebElement txtDocumentTitle;

	@FindBy(id = "fimDocuments_fimComplaint0fimDocumentAttachment")
	public WebElement txtAttachment;

	@FindBy(id = "fimComplaint_0fimTtComplaintBy")
	public WebElement txtComplainantName;

	@FindBy(id = "address_fimComplaint0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_fimComplaint0city")
	public WebElement txtCity;

	@FindBy(id = "address_fimComplaint0country")
	public WebElement drpCountry;

	@FindBy(id = "address_fimComplaint0zipcode")
	public WebElement txtPostalCode;

	@FindBy(id = "address_fimComplaint0state")
	public WebElement drpState;

	@FindBy(id = "address_fimComplaint0emailIds")
	public WebElement txtEmailId;

	@FindBy(id = "address_fimComplaint0phoneNumbers")
	public WebElement txtPhone;

	@FindBy(id = "submitButton")
	public WebElement btnSubmit;

}
