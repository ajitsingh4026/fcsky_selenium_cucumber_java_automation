package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageAreaRegionUI {
	
	@FindBy(xpath=".//input[@type='button' and @value='Add Area / Region']")
	public WebElement addAreaRegion_btn;
	
	public ManageAreaRegionUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
		
	

}
