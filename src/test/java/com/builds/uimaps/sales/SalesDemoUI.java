package com.builds.uimaps.sales;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import fcsky.automation.WebDriverFactory;

public class SalesDemoUI extends WebDriverFactory{

	// add Action Itemre 
	@FindBy(id = "addsectionlink")
	public WebElement addActionItem;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(id = "priority")
	public WebElement selectPriority;

	@FindBy(id = "scheduleCompletionDays")
	public WebElement scheduleCompletionDays;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(xpath = ".//input[@name='buttonSubmit']")
	public WebElement addBtn ;

	@FindBy(id = "resultsPerPage")
	public WebElement resultsPerPage;

	@FindBy(id = "auditAdminTopSearchString")
	public WebElement auditAdminTopSearchString;

	@FindBy(xpath = ".//*[@alt='Search Action Item']")
	public WebElement searchActionItem ;

	public SalesDemoUI() {
		PageFactory.initElements(driver, this);
	}
}
