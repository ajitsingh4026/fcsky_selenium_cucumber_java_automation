package com.builds.uimaps.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CommonUI {

	/*
	 * Common elements used accross all modules
	 */

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement ADD_MORE_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Complete']")
	public WebElement Complete_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Log a Task' or @value='Log A Task']")
	public WebElement LogATask_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Log a Call' or @value='Log A Call']")
	public WebElement LogACall_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Search']")
	public WebElement Search_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Send Email']")
	public WebElement SendEmail_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Add To Group']")
	public WebElement Add_To_Group_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Delete' or .='Delete']")
	public WebElement Delete_Input_ByValue;

	@FindBy(xpath = ".//button[@value='Delete' or .='Delete']")
	public WebElement Delete_Button_ByValue;

	@FindBy(xpath = ".//input[@value='Print']")
	public WebElement Print_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement Save_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement Submit_Input_ByValue;

	@FindBy(xpath = ".//button[.='Save']")
	public WebElement Save_Button_ByValue;

	@FindBy(xpath = ".//button[.='Submit']")
	public WebElement Submit_Button_ByValue;

	@FindBy(xpath = ".//input[@value='Reset']")
	public WebElement Reset_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Add' or @value='ADD']")
	public WebElement Add_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement Cancel_Input_ByValue;

	@FindBy(xpath = ".//button[.='Cancel']")
	public WebElement Cancel_Button_ByValue;

	@FindBy(xpath = ".//button[.='Modify']")
	public WebElement modify_Button_ByValue;

	@FindBy(xpath = ".//input[@value='Archive']")
	public WebElement Archive_Input_ByValue;

	@FindBy(xpath = ".//button[.='Archive']")
	public WebElement Archive_Button_ByValue;

	@FindBy(xpath = ".//input[@value='Unarchive']")
	public WebElement Unarchive_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Next' or @value='NEXT']")
	public WebElement Next_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Prev' or @value='Previous' or @value='PREV' or @value='PREVIOUS']")
	public WebElement Previous_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Export as Excel' or @value='Export As Excel' or @value='Export To Excel' or @value='Export to Excel']")
	public WebElement Export_as_Excel_Input_ByValue;;

	@FindBy(xpath = ".//input[@value='Create']")
	public WebElement Create_Input_ByValue;

	@FindBy(xpath = ".//button[@value='Create' or @value='CREATE'] or contains(text(),'Create')")
	public WebElement Create_Button_ByValue;

	@FindBy(xpath = ".//input[.='Close' or @name='Close' or @value='Close']")
	public WebElement Close_Input_ByValue;

	@FindBy(xpath = ".//button[.='Close' or @name='Close' or @value='Close']")
	public WebElement Close_Button_ByValue;

	@FindBy(xpath = ".//input[@value='More']")
	public WebElement More_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Continue']")
	public WebElement Continue_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement Back_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Create New Report']")
	public WebElement Create_New_Report_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Save View']")
	public WebElement Save_View_Input_ByValue;

	@FindBy(xpath = ".//input[@value='View Report']")
	public WebElement View_Report_Input_ByValue;

	@FindBy(xpath = ".//input[@value='View in Map']")
	public WebElement View_In_Map_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Associate with Campaign' or @value='Associate With Campaign']")
	public WebElement Associate_With_Campaign_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Create Workflow']")
	public WebElement Create_Workflow_Input_ByValue;

	@FindBy(xpath = ".//input[@value='Send FDD']")
	public WebElement Send_FDD_Input_ByValue;

	@FindBy(xpath = ".//div[@data-role='ico_Filter']")
	public WebElement ico_Filter_XPATH_ByDataRole;

	@FindBy(id = "searchTemplate")
	public WebElement searchTemplate_Input_ByID;

	@FindBy(id = "search")
	public WebElement Search_Button_ByID;

	@FindBy(xpath = ".//button[@value='Apply Filters']")
	public WebElement ApplyFilters_Button_ByValue;

	@FindBy(id = "searchButton")
	public WebElement Search_Input_ByID;

	//public String frameClass_newLayoutcboxIframe = "newLayoutcboxIframe";
	
	/*
	@FindBy(className="newLayoutcboxIframe")*/
	public String frameClass_newLayoutcboxIframe = "newLayoutcboxIframe";

	/*
	@FindBy(id="cboxIframe")
	public WebElement cboxIframe;*/
	
	public String cboxIframe = "cboxIframe";	
	
	
	@FindBy(id="cboxIframe")
	public WebElement cbox;
	
	@FindBy(xpath = ".//input[@value='Yes']")
	public WebElement yes_Input_ByValue;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement no_Input_ByValue;

	@FindBy(xpath = ".//button[@value='Yes']")
	public WebElement yes_Button_ByValue;

	@FindBy(xpath = ".//button[@value='No']")
	public WebElement no_Button_ByValue;

	@FindBy(id = "showQuickLinks")
	public WebElement showQuickLinks;

	@FindBy(xpath = ".//*[@id='hideBar']/img")
	public WebElement hideNotificationBar ;

	@FindBy(xpath = ".//*[@id='showBar']/img")
	public WebElement showNotificationBar ;

	@FindBy(xpath = ".//*[@id='contentQuickLinks']//a[contains(text(),'Send Alert')]")
	public WebElement showAlertQuickLink ;

	@FindBy(xpath = ".//*[@id='contentQuickLinks']//a[contains(text(),'Create New Group')]")
	public WebElement createNewGroupQuickLink ;
	
	@FindBy(xpath = ".//*[@id='contentQuickLinks']//a[contains(text(),'Add Library Document')]")
	public WebElement addLibraryDocumentLink ;
	
	@FindBy(xpath = ".//*[@id='contentQuickLinks']//a[contains(text(),'Add News')]")
	public WebElement addNewsQuickLink;

	@FindBy(xpath = ".//div[@id='cboxClose']")
	public WebElement cboxClose_Div_ByID;

	@FindBy(xpath = ".//input[@value='Ok' or @value='OK']")
	public WebElement ok_Input_ByValue;

	@FindBy(xpath = ".//button[@value='Ok' or @value='OK']")
	public WebElement ok_Button_ByValue;

	public CommonUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
